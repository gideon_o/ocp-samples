package se.bbs.chapter_07;

import se.bbs.chapter_06.TryAndTryWithResources;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;

public class CreateAndRunThreads {

    static void justSomeThingToRun() {
        try {
            Random random = new Random();
            random.nextLong();
            /**
             * Pauses the execution for the given time.
             * */
            Thread.sleep(random.nextInt(1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        IntStream.range(1, 10).forEach((n) -> System.out.println(Thread.currentThread().getName() + " number: " + n));
    }

    static void justSomeThingToRunCallable() throws IOException {
        URL filePath = TryAndTryWithResources.class.getClassLoader().getResource("some_text_file.txt");
        Files.newBufferedReader(Paths.get(filePath.toString()));
    }

    class MyThreadClass extends Thread {

        @Override
        public void run() {
            justSomeThingToRun();
        }
    }

    class MyRunnableClass implements Runnable {

        @Override
        public void run() {
            justSomeThingToRun();
        }
    }

    void simpleThread() {
        /**
         * Extending Thread class should be used if
         * - context bases rules are need when thread is created
         * */
        MyThreadClass myThreadClass = new MyThreadClass();

        /**
         * A priority can be set on the thread to indicate how important it is, higher priority receives additional clock cycles.
         * */
        myThreadClass.setPriority(Thread.MIN_PRIORITY);
        myThreadClass.start();

        /**
         * Implementing runnable class i preferable
         * - Makes it possible to extend a different class
         * - Better object-oriented design practices
         * - Makes it more compatible with Concurrent API
         * */
        MyRunnableClass myRunnableClass = new MyRunnableClass();
        new Thread(myRunnableClass).start();

        /**
         * Example using runnable function interface and method reference
         * */
        new Thread(CreateAndRunThreads::justSomeThingToRun).start();
    }

    void executorService() {
        /**
         * Simplest executor service runs task synchronously as added.
         * */
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        /**
         * Methods for the exam
         * */
        singleThreadExecutor.execute(CreateAndRunThreads::justSomeThingToRun);

        /**
         * Takes runnable interface
         * */
        Future<?> submit = singleThreadExecutor.submit(CreateAndRunThreads::justSomeThingToRun);

        /**
         * Takes callable interface
         * */
        Future<Object> submit1 = singleThreadExecutor.submit(() -> { throw new Exception(); });

        try {
            /**
             * Takes list of callable
             * - executes and returns in order
             * */
            List<Future<Object>> futures = singleThreadExecutor.invokeAll(
                    asList(
                            () -> {
                                throw new Exception();
                            },
                            () -> {
                                throw new Exception();
                            })
            );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            /**
             * Takes list of callable
             * - executes in order but returns the task that finished first.
             * - all other task are cancelled
             * */
            Object o = singleThreadExecutor.invokeAny(
                    asList(
                            () -> {
                                throw new Exception();
                            },
                            () -> {
                                throw new Exception();
                            })
            );
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        /**
         * It important to shutdown executor after us to avoid memory leak.
         * */
        singleThreadExecutor.shutdown();
        boolean isShuttingDown = singleThreadExecutor.isShutdown();
        if (isShuttingDown) {
            /**
             * All submitted task will be executed but
             * any task added in this state will result in RejectedExecutionException
             * */

            boolean isTerminated = singleThreadExecutor.isTerminated();
            if (isTerminated) {
                System.out.println("executor service has shutdown");
            }
        }

    }

    /**
     * take notes from page 340
     * */

    void computableFuture() {
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        /**
         * Future<V> object provides the current state of the provided task
         * */
        Future<?> future = singleThreadExecutor.submit(CreateAndRunThreads::justSomeThingToRun);

        boolean done = future.isDone();

        boolean cancelled = future.isCancelled();

        boolean shouldInterruptTask = true;
        /**
         * Cancels an ongoing task
         * */
        future.cancel(shouldInterruptTask);

        try {
            Object o = future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        try {
            /**
             * Same as get but throws an additional TimeoutException.
             * */
            long timeout = 2000;
            future.get(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }

    }

    public static void use(Supplier<Integer> expression) {}
    public static void use(Callable<Integer> expression) {}

    void callableInterface() {
        /**
         * Callable differs from Runnable in that, it can throw a checked exception and return a value.
         * */
        Callable<Connection> callable = () -> DriverManager.getConnection("some url");

        /**
         * Be ware when lambda interfaces are provided to a method.
         * The method call results in a "Ambiguous method call." For this case, there is not difference between Callable and Supplier.
         *
         * - java dose not take the body into consideration
         * */
//        use(() -> {throw new IOException();});

        /**
         * A possible workaround is to explicitly cast the lambda expression
         * */
        use((Callable) () -> {throw new IOException();});

        /**
         * Maybe add Runnable example
         * */
    }

    void scheduledExecutorService() {
        /**
         * The scheduled executor service is used to call a task on a set interval.
         *
         * - Be ware, if the executor is discarded before the delay, no task will be executed
         * */
        ScheduledExecutorService scheduledSingleThreadExecutor = Executors.newSingleThreadScheduledExecutor();

        long delay = 2000;

        /**
         * Common scheduled method takes Runnable or Callable with TimeUnit interval.
         * */
        ScheduledFuture<?> scheduledFuture = scheduledSingleThreadExecutor.schedule(() -> System.out.println("some task"), delay, TimeUnit.MILLISECONDS);

        ScheduledFuture<Object> scheduledFuture2 = scheduledSingleThreadExecutor.schedule(() -> {
            throw new Exception();
        }, delay, TimeUnit.MILLISECONDS);

        long period = 1000;

        /**
         * Run task every period after the initial delay. The task is run regardless of weather the previous task finished.
         * */
        scheduledSingleThreadExecutor.scheduleAtFixedRate(() -> { System.out.println("some task"); }, delay, period, TimeUnit.MILLISECONDS);

        /**
         * same as scheduleAtFixedRate with the addition of waiting for the previous task to finish
         * */
        scheduledSingleThreadExecutor.scheduleWithFixedDelay(() -> { System.out.println("some task"); }, delay, period, TimeUnit.MILLISECONDS);


        /**
         * ScheduledFuture delay differs from Future in that it provides a ScheduledFuture.getDelay method
         *
         * - returns the remaining time by time unit
         * */
        long delay1 = scheduledFuture.getDelay(TimeUnit.MILLISECONDS);
    }


    /**
     * OBS! Be ware of assigned interface for a given executor service.
     * */
    void pooledExecutorServices() {
        int availableProcessors = Runtime.getRuntime().availableProcessors();

        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();

        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

        /**
         * Executor that create new threads as need and also reuses previously created threads.
         * */
        ExecutorService service = Executors.newCachedThreadPool();

        /**
         * Create a fixed nr of thread pools,
         *
         * - use Runtime.getRuntime().availableProcessors(); to determine, nr of available threads
         * */
        ExecutorService service1 = Executors.newFixedThreadPool(availableProcessors);

        /**
         * Create scheduled executor service, with fixed pool size
         * */
        ScheduledExecutorService scheduledExecutorService1 = Executors.newScheduledThreadPool(availableProcessors);
    }

    public static void main(String[] args) {
//        CreateAndRunThreads createAndRunThreads = new CreateAndRunThreads();
//        createAndRunThreads.simpleThread();
    }
}
