package se.bbs.chapter_07;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SynchronizingData {

    int count = 0;
    void incrementCount() {
        System.out.print(++count + " ");
    }

    /**
     * When the or more thread operate on value concurrently resulting in a unexpected output is
     * referred to as a Race condition
     * */
    void raceCondition() {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        ExecutorService service = Executors.newFixedThreadPool(availableProcessors);
        for (int i = 0; i < availableProcessors; i++) {
            service.submit(() -> incrementCount());
        }
    }

    public static void main(String[] args) {
        SynchronizingData synchronizingData = new SynchronizingData();
        synchronizingData.raceCondition();
    }
}
