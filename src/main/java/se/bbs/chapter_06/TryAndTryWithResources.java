package se.bbs.chapter_06;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.function.Predicate;

public class TryAndTryWithResources {

    static URL filePath = TryAndTryWithResources.class.getClassLoader().getResource("some_text_file.txt");

    void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    void validTryStatement() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("url");
            /**
             * Try clause must have ether catch or finally clause
             * - can also have multiple catch clauses but be mindful if a exceptions is a child class of the on above
             * as this will result in unreachable code
             * */
        } catch (SQLException exception) {
            exception.printStackTrace();
            /**
             * Finally claus is always called even if an exception is thrown
             * */
        } finally {
            assert connection != null;
            closeConnection(connection);
        }
    }

    void unreachableCatchClause() {
        try {
            Connection connection = DriverManager.getConnection("url");
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        /**
         * this row results in a compile error as the catch clause is unreachable
         * */
//        catch (SQLException exception) {
//
//        }
    }

    void methodNeedsToThrowCheckedException() {
        try {
            System.out.println("does something");


        } /**
         * Method does not have the potential to throw checked exception, resulting i compile error for SqlException.
         * */
//        catch (SQLException | NullPointerException e ){ }

        finally {
            System.out.println("finally clause");
        }

    }

    /**
     * - throws keyword is used to indicate that a method throws a exception
     * - throw keyword is used to throw an exception
     */
    void throwsInMethodSignature() throws IOException {
        throw new IOException();
    }

    /**
     * - Method implementation is not required to throw the declared exception
     * - The caller still needs to handle exception if declared in in method signature
     */
    void methodDoesNotThrow() throws IOException {

    }


    /**
     * the calling method needs to handle a checked exception if defined in method signature
     */
    void callerNeedsToHandleException() {
        try {
            throwsInMethodSignature();
        } catch (IOException e) {
            /**
             * calling printStackTrace() is not required as Java will automatically print a handle exception
             * - this example is when us explicitly call the method
             * */
            e.printStackTrace();
        }
        /**
         * this line results in compile error as checked exceptions still needs to be handled
         * */
//        methodDoesNotThrow();
    }

    /**
     * Declaring a custom checked exception
     * - By extending Exception class
     */
    class MyCheckedException extends Exception {
        /**
         * The first 3 constructors ar mose common to implement
         */
        public MyCheckedException() {
        }

        public MyCheckedException(String message) {
            super(message);
        }

        public MyCheckedException(String message, Throwable cause) {
            super(message, cause);
        }

        public MyCheckedException(Throwable cause) {
            super(cause);
        }

        public MyCheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

    class MyRunTimeException extends RuntimeException {
        public MyRunTimeException() {
        }

        public MyRunTimeException(String message) {
            super(message);
        }

        public MyRunTimeException(String message, Throwable cause) {
            super(message, cause);
        }

        public MyRunTimeException(Throwable cause) {
            super(cause);
        }

        public MyRunTimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

    void multiCatch() {
        try {
            Path path = Paths.get(filePath.toString());
            String text = new String(Files.readAllBytes(path));
            LocalDate date = LocalDate.parse(text);
            System.out.println(date);
            /**
             * Multi-catch clause syntax:
             * - Be mindful for the single exception variable, that can be ether instance of DateTimeParseException or IOException
             * */
        } catch (DateTimeParseException | IOException e) {
            e.printStackTrace();
            /**
             * Keep in mind, exception variable is effectively final i.e cant be reassigned.
             * Will result in compile error
             * */
//            e = new RuntimeException();
            throw new RuntimeException(e);
        }
        /**
         * this does not compile because of the child parent relations.
         * */
//        catch (FileNotFoundException | IOException e) {
//
//        }
    }

    void validTryWithResources() throws IOException {
        /**
         * try-with-resources also known as automatic resource management as java takes care of properly closing
         * resources.
         *
         * - catch clause is not required
         * - finally clause in not required, but is implicitly used by java to close resources.
         * */
        try(BufferedReader r = Files.newBufferedReader(Paths.get(filePath.toString()));
            BufferedWriter w = Files.newBufferedWriter(Paths.get(filePath.toString()))) {
        }
        /**
         * code will compile if catch and finally are present, but this is not necessary.
         *
         * - the implicit finally claus is still run before any user defined.
         * */
//        catch (IOException e) {
//
//        } finally {
//
//        }
    }

    void customerClosableClass()  {
        /**
         * The order of try-with-resources is as follows
         * - resources create
         * - try clause runs
         * - close method is called
         * - catch clause if exceptions is thrown
         * - finally clause if declared
         * */
        try(MyClosableClass myClosableClass = new MyClosableClass()) {

            /**
             * If implementation of the close method throws a checked exception, this need to be handled or declared in the method signature.
             * */
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    class MyClosableClass implements AutoCloseable {
        /**
         * Obs! when the close method is declared with a check exception, this needs to be handle by the caller.
         * - close method is called when references is out of scope.
         * */
        public void close()throws SQLException {
            DriverManager.getConnection("someurl");
        }
    }

    /**
     * the concept of suppressed exceptions occurs when multiple exceptions a thrown
     * - the first exception is designated as the primary exception and all other as suppressed
     */
    void suppressedExceptions() {
        /**
         * If no exception are throw from try claus, myClosableClass2 will be marked as the primary exception.
         * This is because try-with-resource closes resources backwards
         * */
        try(MyClosableClass myClosableClass = new MyClosableClass();
            MyClosableClass myClosableClass2 = new MyClosableClass()) {

            /**
             * in this example the following exception is marked as the primary exception. The 2 exceptions from
             * MyClosableClass are suppressed.
             * */
            throw new SQLException();
        } catch (SQLException e) {
            for (Throwable ex: e.getSuppressed()) {
                ex.printStackTrace();
            }
        } finally {
            /**
             * suppressed exceptions only applies to exceptions in the try clause, if this happens all other exceptions are lost.
             * */
            throw new RuntimeException();

        }
    }

    /**
     * A common pattern is to log an exception before throwing re-throwing the exception
     * */
    static void reThrowingException() throws IOException, SQLException {
        try {
            DriverManager.getConnection("some url");
            Files.newBufferedReader(Paths.get(filePath.toString()));

            /**
             * Import to know, only the exceptions decleared in the signature are allowed to be re-thrown
             * */
//            throw new ClassNotFoundException();
        } catch (Exception e) {
            e.printStackTrace();
            /**
             * catching an re-throwing exception
             * */
            throw e;
        }
        /**
         * To avoid code duplication the above catch clause can be used instead. Making it easier to add new exceptions int the future.
         * */
//        catch (IOException | SQLException e) {
//            e.printStackTrace();
//            throw e;
//        }
    }

    void validAssert() {
        /**
         * assertion syntax takes an boolean expression and optional error message
         * - if assertions fails and AssertionError is thrown
         * - assert are disabled by default
         * - to enable java -enableassertions <class-name> or <package-name...>, i.e java -enableassertions se.bbs...
         *  - the dots (...) denotes all subclasses
         *  - optional -ea <class-name> or <package-name>
         *  - assertions can be disabled with java -da <class-name>
         * */
        assert (5 > 10): "this an optional error message";
    }
}
