package se.bbs.chapter_10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class SetupPostgresDatabase {
    static String vendor = "postgresql";
    static String host = "localhost";
    static String databaseName = "ocp-db";
    static String url = "jdbc:"+ vendor +"://" + host +"/" + databaseName;

    static String username = "ocp_user";
    static String password = "password123"; //yes i know

    static void init() throws SQLException {
        try(Connection connection = DriverManager.getConnection(url, username, password)) {
            Statement statement = connection.createStatement();

            Map<String, String> tables = new HashMap<String, String>() {{
                put("species", "CREATE TABLE species (id INTEGER PRIMARY KEY, name VARCHAR(255),num_acres DECIMAL)");
                put("animal", "CREATE TABLE animal (id INTEGER PRIMARY KEY, species_id integer, name VARCHAR(255), date_born TIMESTAMP)");
            }};

            createInitSchema(statement, tables);
        }
    }

    static boolean tableExists(Statement statement, String tableName) throws SQLException {
        boolean hasRows = statement.execute("SELECT EXISTS (\n" +
                "   SELECT FROM information_schema.tables \n" +
                "   WHERE  table_schema = 'public'\n" +
                "   AND    table_name   = '"+ tableName +"'\n" +
                "   );");

        if (!hasRows) {
            return false;
        }

        ResultSet resultSet = statement.getResultSet();

        if (!resultSet.next()) {
            return false;
        }

        return resultSet.getBoolean("exists");
    }

    static void createInitSchema(Statement statement, Map<String, String> tables) throws SQLException {
        for (Map.Entry<String, String> entry : tables.entrySet()) {

            String name = entry.getKey();

            boolean exists = tableExists(statement, name);

            if (exists) {
                statement.executeUpdate("DROP TABLE " + name);
            }

            String createTableSql = entry.getValue();

            statement.executeUpdate(createTableSql);
        }
    }

    public static void main(String[] args) throws Exception {
        init();

//        try (Connection conn = DriverManager.getConnection(url, null, null);
//             Statement stmt = conn.createStatement()) {
//            stmt.executeUpdate("CREATE TABLE species (id INTEGER PRIMARY KEY, name VARCHAR(255),num_acres DECIMAL)");
//
//
//            stmt.executeUpdate("INSERT INTO species VALUES (1, 'African Elephant', 7.5)");
//            stmt.executeUpdate("INSERT INTO species VALUES (2, 'Zebra', 1.2)");
//
//            stmt.executeUpdate("INSERT INTO animal VALUES (1, 1, 'Elsa', '2001−05−06 02:15')");
//            stmt.executeUpdate("INSERT INTO animal VALUES (2, 2, 'Zelda', '2002−08−15 09:12')");
//            stmt.executeUpdate("INSERT INTO animal VALUES (3, 1, 'Ester', '2002−09−09 10:36')");
//            stmt.executeUpdate("INSERT INTO animal VALUES (4, 1, 'Eddie', '2010−06−08 01:24')");
//            stmt.executeUpdate("INSERT INTO animal VALUES (5, 2, 'Zoe', '2005−11−12 03:44')");
//
//        }
    }
}
