package se.bbs.chapter_10;

import java.sql.*;


/**
 * - Driver, connects to database
 * - Connection, communicates with database
 * - Statement, runs statements
 * - ResultSet, handles response from select query.
 */

public class JDBC_interfaces {

    /**
     * Establishes a JDBC connection with vendor specific Driver
     * Requirements
     * - Vendor driver in classpath
     * - Valid url i.e jdbc:<vendor>:<url-location>,jdbc:postgresql://localhost/database-name
     * other signature: DriverManager.getConnection(url, name, password)
     */
    Connection getConnection() throws ClassNotFoundException, SQLException {
        /**
         * May be included by not required
         * - was required in jdbc 3 */
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection(SetupPostgresDatabase.url);
    }

    Statement getStatement(Connection jdbcConnection) throws SQLException {
        /**
         * When requiring a statement, parameters can be passed.
         * @param resultSetType
         *  - ResultSet.TYPE_FORWARD_ONLY, accesses from start to end
         *  - ResultSet.TYPE_SCROLL_SENSITIVE accesses data in any order or random access
         *  - ResultSet.TYPE_SCROLL_INSENSITIVE accesses data in any order or random access, also picks up changes to database
         *  ResultSetType will be downgraded if the selected is not available
         *
         * @param resultSetConcurrency
         *  - ResultSet.CONCUR_READ_ONLY default, resultSet is immutable
         *  - ResultSet.CONCUR_UPDATABLE, resultSet is mutable, not supported by all drivers
         * */
        return jdbcConnection.createStatement();
    }

    void mutatingStatements(Statement stmt) throws SQLException {
        int result = stmt.executeUpdate(
                "INSERT INTO species VALUES (10, 'Deer', 3)");

        result = stmt.executeUpdate(
                "UPDATE species SET name = '' WHERE name = 'None'");

        result = stmt.executeUpdate(
                "DELETE FROM species WHERE id = 10");
    }

    void selectStatement(Statement stmt) throws SQLException {
        ResultSet rs = stmt.executeQuery("select * from species");
    }

    ResultSet executeMethod(Statement stmt) throws SQLException {
        boolean hasRows = stmt.execute("select * from species");

        ResultSet rs = null;

        if (hasRows) {
            rs = stmt.getResultSet();
            System.out.println("ran a query");
        } else {
            int result = stmt.getUpdateCount();
            System.out.println("ran an update");
        }

        return rs;
    }

    void executeStatements() throws SQLException, ClassNotFoundException {
        Statement stmt = getStatement(getConnection());

        mutatingStatements(stmt);

        selectStatement(stmt);
    }

    /**
     * Proper order is required when extracting data from result set. Keep in mind:
     * - Forgetting to call rs.next() "or any other row changing method" will result in SqlException.
     * - Column out of rang or unknown column will result in will SqlException
     */
    void handleResultSet() throws SQLException, ClassNotFoundException {
        ResultSet rs = executeMethod(getStatement(getConnection()));

        boolean hasNextRow = rs.next();

        if (hasNextRow) {
            rs.getInt("columnNameOrIndexValue");
        }
    }

    void handleResultSetRandomAccesses() throws SQLException, ClassNotFoundException {
        Statement stmt = getConnection()
                .createStatement(
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY
                );

        ResultSet rs = stmt.executeQuery("SELECT id FROM species ORDER BY id");

        /**
         * When passing resultType i.e TYPE_SCROLL_SENSITIVE, TYPE_SCROLL_INSENSITIVE
         * use the following methods to navigate
         * - resultSet.beforeFirst(), returns void
         * - resultSet.first()
         * - resultSet.last()
         * - resultSet.afterLast(), returns void
         * - resultSet.previous()
         * */
        rs.beforeFirst();
        boolean first = rs.first();
        boolean last = rs.last();
        rs.afterLast();
        boolean previous = rs.previous();

        /**
         * resultSet.absolute(int) can be used to jump any row.
         * Be mindful for
         * - rows starts at 1 index
         * - out of range will result in SqlException
         * - negative number start the point from end and backwards
         * */
        rs.absolute(0);

        /**
         * resultSet.absolute(int) can be used to jump to row relative from current row
         * - out of range will result in SqlException
         * - negative numbers moves backward
         * */
        rs.relative(2);
    }

    /**
     * Close resource order is as follows:
     * - resultSet
     * - statement
     * - connection
     */
    void closingResources() throws SQLException {
        /**
         * Try-with-resources closes in the revers order, as state above. Keep in mind that closing resources is not strictly
         * necessary. Also, when closing resources:
         * - Connection, will close Statement and ResultSet,
         * - Statement, will close ResultSet
         * - Statement, reuse will also close ResultSet
         * */
        try (Connection connection = DriverManager.getConnection(SetupPostgresDatabase.url);
             Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
             ResultSet rs = statement.executeQuery("SELECT * FROM animal")) {

            /**
             * Keep in mind that resultSet is automatically closed if statement is reused
             * */
            ResultSet rs2 = statement.executeQuery("SELECT id FROM animal WHERE name='Zoe'");

            /**
             * recap, closing order for this method is as follows:
             * - rs
             * - rs2
             * - statement
             * - connection*/

        }
    }
}
